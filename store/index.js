import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = () => new Vuex.Store({
  state: {
    table: [],
    frequency: [],
    auth: null
  },
  mutations: {
    SET_TABLE: (state, data) => {
      state.table = data;
    },
    SET_FREQUENCY: (state, data) => {
      state.frequency = data.data;
    },
    SET_LOGIN: (state, data) => {
      state.auth = data;
    }
  },
  actions: {
    async GET_TABLE  ({ commit }, page) {
      let data = await this.$axios.$get(`/table?_page=${page}`);
      commit('SET_TABLE', data);
    },
    async GET_FREQUENCY ({ commit }) {
      let data = await this.$axios.get(`/frequency`);
      commit('SET_FREQUENCY', data);
    }
  }
})

export default store