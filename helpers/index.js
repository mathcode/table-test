function nn(str) {
    const reg = new RegExp( "^([0-9]*\[0-9]+)$", "g" );
    return reg.test(str);
}
export {
    nn
}